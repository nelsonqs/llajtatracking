
package st.nelson.com.appllajtatraking;


public class TrackingBean {

    private Integer seq;
    private Float lat;
    private Float lon;
    private String typ;
    private String stpid;
    private String stpnm;
    private Integer pdist;

    public TrackingBean(Integer seq, Float lat, Float lon, String typ, String stpid, String stpnm, Integer pdist) {
        this.seq = seq;
        this.lat = lat;
        this.lon = lon;
        this.typ = typ;
        this.stpid = stpid;
        this.stpnm = stpnm;
        this.pdist = pdist;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getStpid() {
        return stpid;
    }

    public void setStpid(String stpid) {
        this.stpid = stpid;
    }

    public String getStpnm() {
        return stpnm;
    }

    public void setStpnm(String stpnm) {
        this.stpnm = stpnm;
    }

    public Integer getPdist() {
        return pdist;
    }

    public void setPdist(Integer pdist) {
        this.pdist = pdist;
    }

}
