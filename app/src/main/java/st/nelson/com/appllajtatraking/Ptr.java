
package st.nelson.com.appllajtatraking;

import java.util.ArrayList;
import java.util.List;

public class Ptr {

    private Integer pid;
    private Integer ln;
    private String rtdir;
    private List<TrackingBean> trackingBean = new ArrayList<TrackingBean>();

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getLn() {
        return ln;
    }

    public void setLn(Integer ln) {
        this.ln = ln;
    }

    public String getRtdir() {
        return rtdir;
    }

    public void setRtdir(String rtdir) {
        this.rtdir = rtdir;
    }

    public List<TrackingBean> getTrackingBean() {
        return trackingBean;
    }

    public void setTrackingBean(List<TrackingBean> trackingBean) {
        this.trackingBean = trackingBean;
    }

}
