package st.nelson.com.appllajtatraking;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by tecnosim on 11/9/2017.
 */

public interface EndPointApi {
    @GET(ConstantRestApi.URL_BOOK)
    Call<TrackingBean> getData();
}
