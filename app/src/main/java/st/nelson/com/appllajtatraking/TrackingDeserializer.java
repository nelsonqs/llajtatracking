package st.nelson.com.appllajtatraking;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by tecnosim on 11/18/2017.
 */

public class TrackingDeserializer implements JsonDeserializer<TrackingBean> {

    public TrackingDeserializer() {
    }

    @Override
    public TrackingBean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObjects = ((json.getAsJsonObject().get("bustime-response")).getAsJsonObject().getAsJsonArray("ptr").get(0)).getAsJsonObject();
        JsonArray paymentsArray = jsonObjects.getAsJsonArray("pt");
        TrackingBean data = new TrackingBean(1,
                paymentsArray.get(0).getAsJsonObject() != null ? ((paymentsArray.get(0).getAsJsonObject()).get("lat")).getAsFloat() : 0
                , paymentsArray.get(0).getAsJsonObject() != null ? ((paymentsArray.get(0).getAsJsonObject()).get("lon")).getAsFloat() : 0
                , "typ"
                , "stip"
                , "stpm"
                , 0);
        return data;
    }
}
