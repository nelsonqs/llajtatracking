package st.nelson.com.appllajtatraking;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by tecnosim on 11/19/2017.
 */


public class SessionManage {
private SharedPreferences pref;
private SharedPreferences.Editor editor;
private static final String PREF_NAME = "LLajta Tracking";
private Context _context;
private int PRIVATE_MODE = 0;
private String KEY_LATITUDE = "LATITUDE";
private String KEY_LONGITUDE = "LONGITUDE";

    public SessionManage(Context _context) {
        this._context = _context;
        this.pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.editor = pref.edit();
    }

    public void saveLocation(String latitud, String longitud){
        this.editor.putString(KEY_LATITUDE,latitud);
        this.editor.putString(KEY_LONGITUDE,longitud);
        this.editor.commit();
    }

    public String getLongitude() {
        return pref.getString(KEY_LONGITUDE,null);
    }
    public String getLatitude() {
        return pref.getString(KEY_LATITUDE,null);
    }
}
