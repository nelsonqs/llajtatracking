package st.nelson.com.appllajtatraking;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class AboutActivity extends AppCompatActivity {
    private Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        this.btnBack = findViewById(R.id.abAtras);

        this.btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                AboutActivity.super.onBackPressed();
            }
        });
    }
}
